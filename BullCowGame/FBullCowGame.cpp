#pragma
#include "FBullCowGame.h"
#include <map>
#define TMap std::map

using FText = std::string;
using int32 = int;

FBullCowGame::FBullCowGame()
{
    Reset();
}

void FBullCowGame::Reset()
{
    const FString HIDDEN_WORD = "planets";
    myHiddenWord = HIDDEN_WORD;

    bGameIsWon = false;
    myCurrentTry = 1;
    
    int i = 0;
}

int32 FBullCowGame::GetMaxTries() const
{
    TMap<int32, int32>wordLengthToMaxTries{ {3,4}, {4,5}, {5,10}, {6,13}, {7,40}, {8,60} };
    return wordLengthToMaxTries[myHiddenWord.length()];
}

int32 FBullCowGame::GetCurrentTry() const
{
    return myCurrentTry;
}

bool FBullCowGame::IsGameWon() const
{
    return bGameIsWon;
}

EGuessStatus FBullCowGame::CheckGuessValidity(FString guess) const
{
    if (!isIsogram(guess))
    {
        return EGuessStatus::NOT_ISOGRAM;
    }
    else if (!isLowerCase(guess))
    {
        return EGuessStatus::NOT_LC;
    }
    else if (guess.length() != GetHiddenWordLength())
    {
        return EGuessStatus::TO_SHORT;
    }
    else
    {
        return EGuessStatus::OK;
    }
    return EGuessStatus::OK;
}

int32 FBullCowGame::GetHiddenWordLength() const
{
    return myHiddenWord.length();
}


//receives a valid guess and increments returns count
FBullCowCount FBullCowGame::SubmitValidGuess(FString guess)
{
    myCurrentTry++;
    FBullCowCount bullCowCount;

    int32 wordLength = myHiddenWord.length(); //assuming words are the same length

    for (int32 i = 0; i < wordLength; i++)
    {
        for (int32 j = 0; j < wordLength; j++)
        {
            if (guess[j] == myHiddenWord[i])
            {
                if (i == j)
                {
                    bullCowCount.bulls++;
                }
                else
                {
                    bullCowCount.cows++;
                }
            }
        }
    }
    if (bullCowCount.bulls == wordLength)
    {
        bGameIsWon = true;;
    }
    else
    {
        bGameIsWon = false;
    }
    return bullCowCount;
}

bool FBullCowGame::isIsogram(FString word) const
{
    //consider 0 and 1 letter strings as isograms 
    if (word.length() <= 1)
    {
        return true;
    }

    //setup our map
    TMap<char, bool> LetterSeen;

    for (auto letter : word)
    {
        letter = tolower(letter);

        if (LetterSeen[letter])
        {
            return false;
        }
        else
        {
            LetterSeen[letter] = true;
        }
    }
    return true; //in cases where /0 is entered
}

bool FBullCowGame::isLowerCase(FString word) const
{
    for (auto letter : word)
    {
        if (!islower(letter))
        {
            return false;
        }
    }
    return true;
}
