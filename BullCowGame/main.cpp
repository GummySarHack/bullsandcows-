#pragma
#include <iostream>
#include <string>
#include "FBullCowGame.h"

using FText = std::string;
using int32 = int;
using namespace std;

void Printintro();
void PlayGame();
FText ValidGuess();
bool AskToPlayAgain();
void PrintGameSummary();

FBullCowGame BCGame;

int32 main()
{
    bool bPlayAgain = false;

    do
    {
        Printintro();
        PlayGame();
        bPlayAgain = AskToPlayAgain();
    }
    while (bPlayAgain);

    return 0;
}


void Printintro()
{
    cout <<"\nWelcome to Bulls and Cows, a great game!\n";
    cout << "Welcome to Bulls and Cows, a fun word game.\n";
    cout << std::endl;
    cout << "          }   {         ___ " << std::endl;
    cout << "          (o o)        (o o) " << std::endl;
    cout << "   /-------\\ /          \\ /-------\\ " << std::endl;
    cout << "  / | BULL |O            O| COW  | \\ " << std::endl;
    cout << " *  |-,--- |              |------|  * " << std::endl;
    cout << "    ^      ^              ^      ^ " << std::endl;
    cout << "Can you guess the " << BCGame.GetHiddenWordLength();
    cout << " letter isogram I'm thinking of?\n";
    cout << endl;
}

void PlayGame()
{
    BCGame.Reset();
    int32 maxTries = BCGame.GetMaxTries();

    while(!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= maxTries)
    {
        FText playerGuess = ValidGuess(); 

        FBullCowCount bullCowCount = BCGame.SubmitValidGuess(playerGuess);
        
        cout << "Bulls = " << bullCowCount.bulls;
        cout << "\nCows = " << bullCowCount.cows << endl;
        cout << "Your guess was: " << playerGuess << endl;
        cout << endl;
    }
    PrintGameSummary();
}


FText ValidGuess()
{
    EGuessStatus Status = EGuessStatus::INVALIDE;
    do
    {
        int32 currentTry = BCGame.GetCurrentTry();

        cout << "Number of ties: " << currentTry << " Enter your guess: ";
        FText playerGuess = "";
        getline(cin, playerGuess);

        Status = BCGame.CheckGuessValidity(playerGuess);

        switch (Status)
        {
        case EGuessStatus::TO_SHORT:
            cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word \n";
            break;
        case EGuessStatus::NOT_LC:
            cout << "Please enter a word in lower case\n";
            break;
        case EGuessStatus::NOT_ISOGRAM:
            cout << "Please enter an isogram\n";
            break;
        default:
            return playerGuess;
        }
        cout << endl;
    }
    while(Status != EGuessStatus::OK); 
}

bool AskToPlayAgain()
{
    cout << endl;
    cout << "Do you want to play again (y/n)?";
    FText response = "";
    getline(cin, response);

    return (response[0] == 'y') || (response[0] == 'Y');
}

void PrintGameSummary()
{
    if (BCGame.IsGameWon())
    {
        cout << "WELL YOU WIN!!!!! \n";
    }
    else if (!BCGame.IsGameWon())
    {
        cout << "Awww, better luck next time! \n";
    }
}
