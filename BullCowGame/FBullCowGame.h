#pragma once
#include <string>

using FString = std::string;
using int32 = int;

struct FBullCowCount
{
    int32 bulls = 0;
    int32 cows = 0;
};


enum class EGuessStatus
{
    INVALIDE,
    OK,
    NOT_ISOGRAM,
    TO_SHORT,
    NOT_LC,
};

class FBullCowGame
{
public:

    FBullCowGame();

    int32 GetMaxTries() const;
    int32 GetCurrentTry() const;
    int32 GetHiddenWordLength() const;

    bool IsGameWon() const;
    EGuessStatus CheckGuessValidity(FString) const;
    void Reset(); 

    FBullCowCount SubmitValidGuess(FString);

private:

    int32 myCurrentTry;
    FString myHiddenWord;
    bool bGameIsWon;
    
    bool isIsogram(FString) const;
    bool isLowerCase(FString) const;
};